﻿using System;

namespace VectorSoftwareSudoku
{
    class Program
    {
        static void Main(string[] args)
        {
            var field = new[]
            {
                new[] {7, 8, 4, 1, 5, 9, 3, 2, 6},
                new []  {5, 3, 9, 6, 7, 2, 8, 4, 1},
                new [] {6, 1, 2, 4, 3, 8, 7, 5, 9},
                new [] {9, 2, 8, 7, 1, 5, 4, 6, 3},
                new [] {3, 5, 7, 8, 4, 6, 1, 9, 2},
                new [] {4, 6, 1, 9, 2, 3, 5, 8, 7},
                new [] {8, 7, 6, 3, 9, 4, 2, 1, 5},
                new [] {2, 4, 3, 5, 6, 1, 9, 7, 8},
                new [] {1, 9, 5, 2, 8, 7, 6, 3, 4}
            };
            
            var sudoku = new Sudoku(field);
            Console.WriteLine(sudoku.IsBoardValid());
        }
    }
}
