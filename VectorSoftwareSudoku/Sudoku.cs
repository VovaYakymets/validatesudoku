﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VectorSoftwareSudoku
{
    public class Sudoku
    {
        private readonly int[][] _board;
        public Sudoku(int[][] board)
        {
            _board = board;
        }

        public bool IsBoardValid()
        {
            return HasProperDimensions() && HasProperRows() && 
                   HasProperColumns() && HasProperSquares();
        }
        private bool HasProperDimensions()
        {
           return _board.Length > 1 &&
                _board.Length == _board[0].Length &&
                Math.Sqrt(_board.Length) == (int)Math.Sqrt(_board.Length);
        }
        
        private bool HasProperRows()
        {
            foreach (var row in _board)
            {
                var values = new HashSet<int>();
                for (int j = 0; j < _board.Length; j++)
                {
                    if (!values.Add(row[j]))
                        return false;
                }
            }
            return true;
        }

        private bool HasProperColumns()
        {
            return !_board
                .Select(_ => new HashSet<int>())
                .Where((values, j) => _board.Any(column => !values.Add(column[j])))
                .Any();
        }
        private bool HasProperSquares()
        {
            for (int i = 0; i < _board.Length; i+= (int)Math.Sqrt(_board.Length))
            {
                for (int m = 0; m < _board.Length; m += (int)Math.Sqrt(_board.Length))
                {
                    var values = new HashSet<int>();
                    for (int j = i; j < Math.Sqrt(_board.Length); j++)
                    {
                        for (int k = m; k < Math.Sqrt(_board.Length); k++)
                        {
                            if (!values.Add(_board[j][k]))
                                return false;
                        }
                    }
                }
               
            }
            return true;
        }
    }
}